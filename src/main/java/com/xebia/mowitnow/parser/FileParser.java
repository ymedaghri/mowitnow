package com.xebia.mowitnow.parser;

import com.xebia.mowitnow.parser.lineparser.*;
import com.xebia.mowitnow.parser.resolver.DirectionResolver;
import com.xebia.mowitnow.parser.resolver.InstructionResolver;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.nio.file.Files.lines;

/**
 * Created by ymedaghri on 24/02/2017.
 */
public class FileParser {

    static Pattern zonePattern = Pattern.compile("[0-9]{1,5}\\s{1}[0-9]{1,5}");
    static Pattern mowerPattern = Pattern.compile("[0-9]{1,5}\\s{1}[0-9]{1,5}\\s{1}[A-Z]{1}");
    static Pattern instructionsPattern = Pattern.compile("[A-Z]{1}[A-Z]*");

    static final Function<InstructionResolver, Function<DirectionResolver, Function<String, Parser>>> toParsers = instructionResolver -> directionResolver -> fileLine ->
    {
        if (zonePattern.matcher(fileLine).matches()) {
            return new MowerZoneParser(fileLine);
        }

        if (mowerPattern.matcher(fileLine).matches()) {
            return new MowerParser(fileLine, directionResolver);
        }

        if (instructionsPattern.matcher(fileLine).matches() && instructionResolver.validate(fileLine)) {
            return new InstructionsParser(fileLine, instructionResolver);
        }

        return new IllegalParser(fileLine);
    };

    private final String fileName;
    private final DirectionResolver directionResolver;
    private final InstructionResolver instructionResolver;

    public FileParser(final String fileName, final DirectionResolver directionResolver, final InstructionResolver instructionResolver) {
        this.fileName = fileName;
        this.directionResolver = directionResolver;
        this.instructionResolver = instructionResolver;
    }


    public Optional<ExecutableParser> parse() throws IOException {
        try (Stream<String> stream = lines(Paths.get(fileName))) {
            return stream.map(toParsers.apply(instructionResolver).apply(directionResolver))
                    .reduce(Parser::composeWith)
                    .map(parser -> (ExecutableParser)parser);
        } catch (IOException e) {
            throw e;
        }
    }

}
