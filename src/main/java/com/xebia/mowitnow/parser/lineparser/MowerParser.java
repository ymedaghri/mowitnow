package com.xebia.mowitnow.parser.lineparser;

import com.xebia.mowitnow.domain.direction.Direction;
import com.xebia.mowitnow.domain.mower.Mower;
import com.xebia.mowitnow.domain.position.Position;
import com.xebia.mowitnow.parser.resolver.DirectionResolver;

import java.util.List;
import java.util.Optional;

import static com.xebia.mowitnow.parser.lineparser.ParserTypes.MOWER_PARSER;
import static com.xebia.mowitnow.parser.lineparser.ParserTypes.MOWER_ZONE_PARSER;
import static java.lang.String.valueOf;

/**
 * Created by medaghrialaouiyoussef on 24/02/2017.
 */
public class MowerParser implements Parser {

    private String line;
    private DirectionResolver directionResolver;

    public MowerParser(final String line, final DirectionResolver directionResolver) {
        this.line = line;
        this.directionResolver = directionResolver;
    }


    @Override
    public Parser composeWith(final Parser parser) {
        return new IllegalParser(line);
    }

    @Override
    public Parser bindTo(final Parser parser) {

        if (parser.getType() == MOWER_ZONE_PARSER) {

            String[] lineTokens = line.split(splitterRegex);
            Integer x = Integer.valueOf(lineTokens[0]);
            Integer y = Integer.valueOf(lineTokens[1]);
            Position position = Position.of(x, y);

            Optional<Direction> maybeDirection = directionResolver.findByCode(valueOf(lineTokens[2]));

            if(!maybeDirection.isPresent()){
                return new IllegalParser(line);
            }

            MowerZoneParser mowerZoneParser = (MowerZoneParser) parser;
            mowerZoneParser.addMower(new Mower(position, maybeDirection.get(), mowerZoneParser.getMowingZone()));

            return parser;
        }

        return new IllegalParser(line);
    }

    @Override
    public ParserTypes getType() {
        return MOWER_PARSER;
    }

}
