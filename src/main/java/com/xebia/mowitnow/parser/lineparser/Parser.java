package com.xebia.mowitnow.parser.lineparser;

import java.util.List;

/**
 * Created by medaghrialaouiyoussef on 24/02/2017.
 */
public interface Parser {

    String splitterRegex = "\\s";

    Parser composeWith(Parser parser);
    Parser bindTo(Parser parser);
    ParserTypes getType();
}
