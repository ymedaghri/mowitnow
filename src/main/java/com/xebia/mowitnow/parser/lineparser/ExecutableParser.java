package com.xebia.mowitnow.parser.lineparser;

import java.util.List;

/**
 * Created by medaghrialaouiyoussef on 24/02/2017.
 */
public interface ExecutableParser {
    List<String> execute();
}
