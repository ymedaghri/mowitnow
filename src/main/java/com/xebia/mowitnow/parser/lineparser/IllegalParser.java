package com.xebia.mowitnow.parser.lineparser;

import java.util.List;

import static com.xebia.mowitnow.parser.lineparser.ParserTypes.ILLEGAL_LINE_PARSER;
import static java.util.Arrays.asList;

/**
 * Created by medaghrialaouiyoussef on 26/02/2017.
 */
public class IllegalParser implements Parser, ExecutableParser {

    private String fileLine;

    public IllegalParser(final String fileLine) {

        this.fileLine = fileLine;
    }

    @Override
    public Parser composeWith(final Parser parser) {
        return this;
    }

    @Override
    public Parser bindTo(final Parser parser) {
        return this;
    }

    @Override
    public ParserTypes getType() {
        return ILLEGAL_LINE_PARSER;
    }

    @Override
    public List<String> execute() {
        return asList(String.format("The line %s of the input file is incorrect", fileLine));
    }

    public String getFileLine() {
        return fileLine;
    }
}
