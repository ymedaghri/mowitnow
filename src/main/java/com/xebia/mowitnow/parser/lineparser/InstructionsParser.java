package com.xebia.mowitnow.parser.lineparser;

import com.xebia.mowitnow.domain.mower.Mower;
import com.xebia.mowitnow.parser.resolver.InstructionResolver;

import java.util.List;
import java.util.Optional;

import static com.xebia.mowitnow.parser.lineparser.ParserTypes.INSTRUCTION_PARSER;
import static com.xebia.mowitnow.parser.lineparser.ParserTypes.MOWER_ZONE_PARSER;

/**
 * Created by medaghrialaouiyoussef on 24/02/2017.
 */
public class InstructionsParser implements Parser {

    private String fileLine;
    private InstructionResolver instructionResolver;

    public InstructionsParser(final String fileLine, final InstructionResolver instructionResolver) {

        this.fileLine = fileLine;
        this.instructionResolver = instructionResolver;
    }

    @Override
    public Parser composeWith(final Parser parser) {
        return new IllegalParser(fileLine);
    }

    @Override
    public Parser bindTo(final Parser parser) {
        if (parser.getType() == MOWER_ZONE_PARSER) {
            Optional<Mower> maybeMower = ((MowerZoneParser) parser).getAddedLast();
            if (!maybeMower.isPresent()) {
                return new IllegalParser(fileLine);
            }

            maybeMower.get().addInstructions(instructionResolver.toInstructions(fileLine));
            return parser;
        }

        return new IllegalParser(fileLine);
    }

    @Override
    public ParserTypes getType() {
        return INSTRUCTION_PARSER;
    }


}
