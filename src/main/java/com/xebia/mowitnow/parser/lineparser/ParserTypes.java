package com.xebia.mowitnow.parser.lineparser;

/**
 * Created by medaghrialaouiyoussef on 26/02/2017.
 */
public enum ParserTypes {

    MOWER_ZONE_PARSER,
    MOWER_PARSER,
    INSTRUCTION_PARSER,
    ILLEGAL_LINE_PARSER;

}
