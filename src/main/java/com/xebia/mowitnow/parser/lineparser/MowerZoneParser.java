package com.xebia.mowitnow.parser.lineparser;

import com.xebia.mowitnow.domain.mower.Mower;
import com.xebia.mowitnow.domain.mowingzone.MowingZone;
import com.xebia.mowitnow.domain.position.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.xebia.mowitnow.parser.lineparser.ParserTypes.MOWER_ZONE_PARSER;
import static java.util.stream.Collectors.toList;

/**
 * Created by medaghrialaouiyoussef on 24/02/2017.
 */
public class MowerZoneParser implements Parser, ExecutableParser {

    private final String line;
    private final MowingZone zone;
    private final List<Mower> mowers = new ArrayList<>();

    public MowerZoneParser(final String line) {
        this.line = line;
        zone = toZone(line);
    }

    private MowingZone toZone(final String line) {
        String[] numbers = line.split(splitterRegex);
        Integer x = Integer.valueOf(numbers[0]);
        Integer y = Integer.valueOf(numbers[1]);

        return MowingZone.of(
                Position.of(0, 0),
                Position.of(x, y));
    }


    @Override
    public Parser composeWith(final Parser parser) {
        return parser.bindTo(this);
    }

    @Override
    public Parser bindTo(final Parser parser) {
        return new IllegalParser(line);
    }

    @Override
    public ParserTypes getType() {
        return MOWER_ZONE_PARSER;
    }

    @Override
    public List<String> execute() {
        return mowers.stream().map(mower -> {
            mower.processInstructions();
            return mower.getStatus();
        }).collect(toList());
    }

    public void addMower(final Mower mower) {
        mowers.add(mower);
    }

    public MowingZone getMowingZone() {
        return zone;
    }

    public Optional<Mower> getAddedLast() {
        return mowers.stream().reduce((a, b) -> b);
    }
}
