package com.xebia.mowitnow.parser.resolver;

import com.xebia.mowitnow.domain.instruction.Instruction;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

/**
 * Created by medaghrialaouiyoussef on 25/02/2017.
 */
public class InstructionResolver {

    private final List<Instruction> values;

    public InstructionResolver(Instruction[] values) {
        this.values = asList(values);
    }

    public Optional<Instruction> findByCode(final String code) {
        return values.stream().filter(value -> value.getCode().equals(code)).findFirst();
    }

    public boolean validate(final String fileLine) {
        return fileLine.chars().mapToObj(String::valueOf).filter(s -> !values.contains(s)).findAny().isPresent();
    }

    public List<Instruction> toInstructions(final String fileLine) {
        return fileLine.chars().mapToObj((i) -> findByCode(String.valueOf((char)i)).get()).collect(Collectors.toList());
    }
}
