package com.xebia.mowitnow.parser.resolver;

import com.xebia.mowitnow.domain.direction.Direction;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by medaghrialaouiyoussef on 25/02/2017.
 */
public class DirectionResolver {

    private final Direction[] values;

    public DirectionResolver(Direction[] values) {
        this.values = values;
    }

    public Optional<Direction> findByCode(final String code) {
        return Stream.of(values).filter(value -> value.getCode().equals(code)).findFirst();
    }
}
