package com.xebia.mowitnow.domain.direction;

import com.xebia.mowitnow.domain.position.Position;

/**
 * Created by medaghrialaouiyoussef on 23/02/2017.
 */
public interface Direction {

    Direction plus90Degrees();

    Direction minus90Degrees();

    Position moveFrom(Position position);

    String getCode();
}
