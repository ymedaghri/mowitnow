package com.xebia.mowitnow.domain.direction;

import com.xebia.mowitnow.domain.position.Position;

/**
 * Created by ymedaghri on 23/02/2017.
 */
public enum EnglishCardinalDirections implements Direction {

    NORTH("N") {
        @Override
        public Direction plus90Degrees() {
            return EAST;
        }

        @Override
        public Direction minus90Degrees() {
            return WEST;
        }

        @Override
        public Position moveFrom(final Position position) {
            return position.incrementY();
        }

    }, SOUTH("S") {
        @Override
        public Direction plus90Degrees() {
            return WEST;
        }

        @Override
        public Direction minus90Degrees() {
            return EAST;
        }

        @Override
        public Position moveFrom(final Position position) {
            return position.decrementY();
        }

    }, EAST("E") {
        @Override
        public Direction plus90Degrees() {
            return SOUTH;
        }

        @Override
        public Direction minus90Degrees() {
            return NORTH;
        }

        @Override
        public Position moveFrom(final Position position) {
            return position.incrementX();
        }

    }, WEST("W") {
        @Override
        public Direction plus90Degrees() {
            return NORTH;
        }

        @Override
        public Direction minus90Degrees() {
            return SOUTH;
        }

        @Override
        public Position moveFrom(final Position position) {
            return position.decrementX();
        }

    };

    private final String code;

    EnglishCardinalDirections(final String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

}
