package com.xebia.mowitnow.domain.instruction;

import com.xebia.mowitnow.domain.direction.Direction;

/**
 * Created by ymedaghri on 23/02/2017.
 */
public enum RotatingInstruction implements Rotating {
    D {
        @Override
        public Direction rotateFrom(Direction direction) {
            return direction.plus90Degrees();
        }
    },
    G{
        @Override
        public Direction rotateFrom(Direction direction) {
            return direction.minus90Degrees();
        }
    };

    @Override
    public String getCode() {
        return name();
    }
}
