package com.xebia.mowitnow.domain.instruction;

import com.xebia.mowitnow.domain.location.Location;
import com.xebia.mowitnow.domain.position.Position;
import com.xebia.mowitnow.domain.direction.Direction;

/**
 * Created by medaghrialaouiyoussef on 23/02/2017.
 */
public interface Moving extends Instruction {

    default Location<Position, Direction> executeOn(final Location<Position, Direction> location) {
        Direction direction = location.getDirection();
        Position position = location.getPosition();
        Position nextPosition = direction.moveFrom(position);
        return new Location<>(nextPosition, direction);
    }
}
