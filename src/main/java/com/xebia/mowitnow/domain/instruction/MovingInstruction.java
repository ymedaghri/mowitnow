package com.xebia.mowitnow.domain.instruction;

/**
 * Created by ymedaghri on 23/02/2017.
 */
public enum MovingInstruction implements Moving {
    A;

    @Override
    public String getCode() {
        return name();
    }
}
