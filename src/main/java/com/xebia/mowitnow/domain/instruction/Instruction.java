package com.xebia.mowitnow.domain.instruction;

import com.xebia.mowitnow.domain.direction.Direction;
import com.xebia.mowitnow.domain.location.Location;
import com.xebia.mowitnow.domain.position.Position;

/**
 * Created by ymedaghri on 23/02/2017.
 */
public interface Instruction {

    Location<Position, Direction> executeOn(final Location<Position, Direction> emplacement);

    String getCode();
}
