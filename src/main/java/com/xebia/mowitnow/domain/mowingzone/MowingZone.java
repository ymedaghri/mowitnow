package com.xebia.mowitnow.domain.mowingzone;

import com.xebia.mowitnow.domain.position.Position;

/**
 * Created by medaghrialaouiyoussef on 17/02/2017.
 */
public class MowingZone {

    /*
                 +------+ upper
                 |      |
                 |      |
           lower +------+
     */

    public final Position lower;
    public final Position upper;


    private MowingZone(final Position lower, final Position upper)
    {
        this.lower = lower;
        this.upper = upper;
    }

    public static MowingZone of(final Position lower, final Position upper)
    {
        return new MowingZone(lower, upper);
    }

    @Override
    public String toString() {
        return "MowingZone{" +
                lower +
                ", " +
                upper +
                "}";
    }

    public boolean isPositionAllowed(final Position position) {
        return position.isGreaterEqualThan(lower)
                && position.isLowerEqualThan(upper);
    }

    public Position getLower() {
        return lower;
    }

    public Position getUpper() {
        return upper;
    }
}
