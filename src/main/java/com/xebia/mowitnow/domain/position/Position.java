package com.xebia.mowitnow.domain.position;

/**
 * Created by ymedaghri on 23/02/2017.
 */
public class Position {

    private final int x;
    private final int y;

    public static Position of(final int x, final int y) {
        return new Position(x, y);
    }

    private Position(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public Position incrementX() {
        return of(x + 1, y);
    }

    public Position incrementY() {
        return of(x, y + 1);
    }

    public Position decrementX() {
        return of(x - 1, y);
    }

    public Position decrementY() {
        return of(x, y - 1);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isGreaterEqualThan(Position position) {
        return getX()>=position.getX() && getY()>=position.getY();
    }

    public boolean isLowerEqualThan(Position position) {
        return getX()<=position.getX() && getY()<=position.getY();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Position position = (Position) o;

        if (x != position.x)
            return false;
        return y == position.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "Position{" +
                "x=" + x +
                ", y=" + y +
                "}";
    }
}
