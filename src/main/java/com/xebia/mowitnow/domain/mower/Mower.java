package com.xebia.mowitnow.domain.mower;

import com.xebia.mowitnow.domain.direction.Direction;
import com.xebia.mowitnow.domain.instruction.Instruction;
import com.xebia.mowitnow.domain.location.Location;
import com.xebia.mowitnow.domain.mowingzone.MowingZone;
import com.xebia.mowitnow.domain.position.Position;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

/**
 * Created by ymedaghri on 23/02/2017.
 */
public class Mower {

    private Position position;
    private Direction direction;
    private final MowingZone mowingZone;
    private final List<Instruction> instructions = new ArrayList<>();

    public Mower(final Position initialPosition, final Direction initialDirection, final MowingZone mowingZone) {
        this.position = initialPosition;
        this.direction = initialDirection;
        this.mowingZone = mowingZone;
    }

    public void processInstructions() {
        instructions.stream().forEach(instruction -> {
            Location<Position, Direction> currentLocation = new Location<>(getPosition(), getDirection());
            Location<Position, Direction> nextLocation = instruction.executeOn(currentLocation);
            Position nextPosition = nextLocation.getPosition();
            if (mowingZone.isPositionAllowed(nextPosition)) {
                direction = nextLocation.getDirection();
                position = nextPosition;
            }
        });
    }

    public Position getPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }

    public void addInstructions(final List<Instruction> instructions) {
        this.instructions.addAll(instructions);
    }

    public String getStatus() {
        return format("%s %s %s", position.getX(), position.getY(), direction.getCode());
    }
}
