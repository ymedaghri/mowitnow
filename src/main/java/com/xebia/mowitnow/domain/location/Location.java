package com.xebia.mowitnow.domain.location;

import com.xebia.mowitnow.domain.direction.Direction;
import com.xebia.mowitnow.domain.position.Position;

/**
 * Created by ymedaghri on 23/02/2017.
 */
public class Location<T extends Position, U extends Direction> {
    private final T position;
    private final U direction;

    public Location(T position, U direction) {
        this.position = position;
        this.direction = direction;
    }

    public T getPosition() {
        return position;
    }

    public U getDirection() {
        return direction;
    }
}
