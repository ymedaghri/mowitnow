package com.xebia.mowitnow;

import com.xebia.mowitnow.domain.direction.EnglishCardinalDirections;
import com.xebia.mowitnow.domain.instruction.Instruction;
import com.xebia.mowitnow.domain.instruction.MovingInstruction;
import com.xebia.mowitnow.domain.instruction.RotatingInstruction;
import com.xebia.mowitnow.parser.FileParser;
import com.xebia.mowitnow.parser.lineparser.ExecutableParser;
import com.xebia.mowitnow.parser.lineparser.MowerZoneParser;
import com.xebia.mowitnow.parser.lineparser.Parser;
import com.xebia.mowitnow.parser.resolver.DirectionResolver;
import com.xebia.mowitnow.parser.resolver.InstructionResolver;

import java.io.IOException;
import java.util.Optional;

import static java.util.stream.Stream.concat;
import static java.util.stream.Stream.of;

/**
 * Created by medaghrialaouiyoussef on 24/02/2017.
 */
public class MainProgram {

    public static final Instruction[] listOfAvailableInstructions = concat(of(MovingInstruction.values()), of(RotatingInstruction.values())).toArray(Instruction[]::new);
    public static final EnglishCardinalDirections[] listOfAvailableDirections = EnglishCardinalDirections.values();

    public static void main(String[] args) throws IOException {

        String fileName;

        switch (args.length) {
            case 1:
                fileName = args[0];
                break;
            default:
                System.out.println("This program is intended to work with exactly one argument (a mowing file path)");
                return;
        }

        FileParser fileParser = new FileParser(fileName,
                new DirectionResolver(listOfAvailableDirections),
                new InstructionResolver(listOfAvailableInstructions));

        fileParser.parse().ifPresent(parser -> parser.execute().stream().forEach(System.out::println));
    }
}
