package com.xebia.mowitnow.domain.location;

import com.xebia.mowitnow.domain.direction.Direction;
import com.xebia.mowitnow.domain.location.Location;
import com.xebia.mowitnow.domain.position.Position;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Created by medaghrialaouiyoussef on 24/02/2017.
 */
public class LocationTest {

    @Test
    public void testLocation() {
        //Given
        Position position = mock(Position.class);
        Direction direction = mock(Direction.class);
        Location<Position, Direction> location = new Location<>(position, direction);

        //When -> Then
        assertThat(location).extracting(Location::getPosition).containsExactly(position);
        assertThat(location).extracting(Location::getDirection).containsExactly(direction);
    }
}