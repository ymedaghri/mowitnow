package com.xebia.mowitnow.domain.mower;

import com.xebia.mowitnow.domain.mowingzone.MowingZone;
import com.xebia.mowitnow.domain.position.Position;
import org.junit.Test;

import java.util.Arrays;

import static com.xebia.mowitnow.domain.direction.EnglishCardinalDirections.EAST;
import static com.xebia.mowitnow.domain.direction.EnglishCardinalDirections.NORTH;
import static com.xebia.mowitnow.domain.instruction.MovingInstruction.A;
import static com.xebia.mowitnow.domain.instruction.RotatingInstruction.D;
import static com.xebia.mowitnow.domain.instruction.RotatingInstruction.G;
import static org.junit.Assert.assertEquals;

/**
 * Created by medaghrialaouiyoussef on 14/02/2017.
 */
public class MowerTest {

    @Test
    public void processSequenceTest_1() {
        //Given
        Mower mower = new Mower(Position.of(1, 2), NORTH, defaultMowingZone());

        //When
        mower.addInstructions(Arrays.asList(G, A, G, A, G, A, G, A, A));
        mower.processInstructions();

        //Then
        assertEquals("position is incorrect", Position.of(1, 3), mower.getPosition());
        assertEquals("direction is incorrect", NORTH, mower.getDirection());
    }

    @Test
    public void processSequenceTest_2() {
        //Given
        Mower mower = new Mower(Position.of(3, 3), EAST, defaultMowingZone());

        //When
        mower.addInstructions(Arrays.asList(A, A, D, A, A, D, A, D, D, A));
        mower.processInstructions();

        //Then
        assertEquals("position is incorrect", Position.of(5, 1), mower.getPosition());
        assertEquals("direction is incorrect", EAST, mower.getDirection());
    }

    private MowingZone defaultMowingZone() {
        Position lowerBound = Position.of(0, 0);
        Position upperBound = Position.of(5, 5);
        return MowingZone.of(lowerBound, upperBound);
    }

}