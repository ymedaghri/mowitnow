package com.xebia.mowitnow.domain.direction;

import com.xebia.mowitnow.domain.position.Position;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static com.xebia.mowitnow.domain.direction.EnglishCardinalDirections.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by ymedaghri on 21/02/2017.
 */
public class EnglishCardinalDirectionsTest {

    @Test
    public void testRotateClockWise() {
        assertThat(NORTH.plus90Degrees()).isEqualTo(EAST);
        assertThat(SOUTH.plus90Degrees()).isEqualTo(WEST);
        assertThat(EAST.plus90Degrees()).isEqualTo(SOUTH);
        assertThat(WEST.plus90Degrees()).isEqualTo(NORTH);
    }

    @Test
    public void testRotateCounterClockWise() {
        assertThat(NORTH.minus90Degrees()).isEqualTo(WEST);
        assertThat(SOUTH.minus90Degrees()).isEqualTo(EAST);
        assertThat(EAST.minus90Degrees()).isEqualTo(NORTH);
        assertThat(WEST.minus90Degrees()).isEqualTo(SOUTH);
    }

    @Test
    public void testMoveFrom() {
        //Given
        final Map<Direction, Consumer<Position>> expectedVerifiesByDirection = new HashMap<>();
        expectedVerifiesByDirection.put(NORTH, (position) -> verify(position).incrementY());
        expectedVerifiesByDirection.put(SOUTH, (position) -> verify(position).decrementY());
        expectedVerifiesByDirection.put(EAST, (position) -> verify(position).incrementX());
        expectedVerifiesByDirection.put(WEST, (position) -> verify(position).decrementX());

        //When -> Then
        expectedVerifiesByDirection.forEach((direction, expectedVerify) -> {
            checkThatCorrespondingMethodIsCalled(direction, expectedVerify);
        });

    }

    private void checkThatCorrespondingMethodIsCalled(final Direction direction, final Consumer<Position> expectedVerify) {
        //Given
        Position position = mock(Position.class);

        //When
        direction.moveFrom(position);

        //Then
        expectedVerify.accept(position);
    }

}