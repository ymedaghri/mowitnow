package com.xebia.mowitnow.domain.position;

import org.junit.Test;

import static com.xebia.mowitnow.domain.position.Position.of;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by medaghrialaouiyoussef on 24/02/2017.
 */
public class PositionTest {
    @Test
    public void testPosition() {
        //Given
        Position position = of(3, 4);

        //When -> Then
        assertThat(position).extracting(Position::incrementX).containsExactly(of(3 + 1, 4));
        assertThat(position).extracting(Position::incrementY).containsExactly(of(3, 4 + 1));
        assertThat(position).extracting(Position::decrementX).containsExactly(of(3 - 1, 4));
        assertThat(position).extracting(Position::decrementY).containsExactly(of(3, 4 - 1));
        assertThat(position).extracting(Position::getX).containsExactly(3);
        assertThat(position).extracting(Position::getY).containsExactly(4);
    }

    @Test
    public void testEquals() {
        //Given
        Position position_1 = of(3, 4);
        Position position_2 = of(4, 3);
        Position position_3 = of(3, 4);

        //When -> Then
        assertThat(position_1.equals(position_2)).isFalse();
        assertThat(position_1.equals(position_3)).isTrue();
        assertThat(position_1.equals(position_1)).isTrue();
        assertThat(position_1.equals(null)).isFalse();
        assertThat(position_1.equals("nope")).isFalse();


    }
}