package com.xebia.mowitnow.domain.mowingzone;

import com.xebia.mowitnow.domain.position.Position;
import org.junit.Test;

import static com.xebia.mowitnow.domain.position.Position.of;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ymedaghri on 21/02/2017.
 */
public class MowingZoneTest {

    @Test
    public void testIfPositionAllowed() throws Exception {
        //Given
        Position lowerBound = of(0, 0);
        Position upperBound = of(5, 5);
        MowingZone mowingZone = MowingZone.of(lowerBound, upperBound);

        //When -> Then
        Position position = of(0, 0);
        assertThat(mowingZone.isPositionAllowed(position))
                .as(format("%s is not allowed for %s", position, mowingZone))
                .isTrue();

        //When -> Then
        position = of(3, 3);
        assertThat(mowingZone.isPositionAllowed(position))
                .as(format("%s is not allowed for %s", position, mowingZone))
                .isTrue();

        //When -> Then
        position = of(5, 5);
        assertThat(mowingZone.isPositionAllowed(position))
                .as(format("%s is not allowed for %s", position, mowingZone))
                .isTrue();
    }

    @Test
    public void testIfPositionNotAllowed() throws Exception {
        //Given
        Position lowerBound = of(0, 0);
        Position upperBound = of(5, 5);
        MowingZone mowingZone = MowingZone.of(lowerBound, upperBound);

        //When -> Then
        Position position = of(6, 3);
        assertThat(mowingZone.isPositionAllowed(position))
                .as(format("%s is allowed for %s", position, mowingZone))
                .isFalse();

    }

}