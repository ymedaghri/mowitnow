package com.xebia.mowitnow.parser;

import com.xebia.mowitnow.domain.mowingzone.MowingZone;
import com.xebia.mowitnow.domain.position.Position;
import com.xebia.mowitnow.parser.lineparser.*;
import com.xebia.mowitnow.parser.resolver.DirectionResolver;
import com.xebia.mowitnow.parser.resolver.InstructionResolver;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.function.Function;

import static com.xebia.mowitnow.parser.FileParser.*;
import static com.xebia.mowitnow.parser.lineparser.ParserTypes.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

/**
 * Created by ymedaghri on 24/02/2017.
 */
public class FileParserTest {

    @Test
    public void regexZonePatternTest() {
        assertThat(zonePattern.matcher("5 5").matches()).isTrue();
        assertThat(zonePattern.matcher("12345 5").matches()).isTrue();
        assertThat(zonePattern.matcher("1 12345").matches()).isTrue();
        assertThat(zonePattern.matcher("").matches()).isFalse();
        assertThat(zonePattern.matcher("5  5").matches()).isFalse();
        assertThat(zonePattern.matcher("5").matches()).isFalse();
        assertThat(zonePattern.matcher("12345").matches()).isFalse();
        assertThat(zonePattern.matcher("-1 10").matches()).isFalse();
    }

    @Test
    public void regexMowerPatternTest() {
        assertThat(mowerPattern.matcher("1 2 N").matches()).isTrue();
        assertThat(mowerPattern.matcher("12345 5 S").matches()).isTrue();
        assertThat(mowerPattern.matcher("1 12345 W").matches()).isTrue();
        assertThat(mowerPattern.matcher("").matches()).isFalse();
        assertThat(mowerPattern.matcher("1 2  N").matches()).isFalse();
        assertThat(mowerPattern.matcher("1 2 N ").matches()).isFalse();
    }

    @Test
    public void regexInstructionsPatternTest() {
        assertThat(instructionsPattern.matcher("ABCDEF").matches()).isTrue();
        assertThat(instructionsPattern.matcher("A").matches()).isTrue();
        assertThat(instructionsPattern.matcher("").matches()).isFalse();
        assertThat(instructionsPattern.matcher("A ").matches()).isFalse();
        assertThat(instructionsPattern.matcher(" ").matches()).isFalse();
        assertThat(instructionsPattern.matcher("AbcDeF").matches()).isFalse();
        assertThat(instructionsPattern.matcher("1").matches()).isFalse();
    }

    @Test
    public void toZoneParserTest() {
        //Given
        Function<String, Parser> uncurriedParser = toParsers.apply(mock(InstructionResolver.class)).apply(mock(DirectionResolver.class));

        //When
        Parser parser = uncurriedParser.apply("5 5");

        //Then
        MowerZoneParser mowerZoneParser = (MowerZoneParser) parser;
        MowingZone mowingZone = mowerZoneParser.getMowingZone();
        assertThat(mowerZoneParser.getType()).isEqualTo(MOWER_ZONE_PARSER);
        assertThat(mowingZone.getLower()).isEqualTo(Position.of(0,0));
        assertThat(mowingZone.getUpper()).isEqualTo(Position.of(5,5));
    }

    @Test
    public void toMowerParserTest() {
        //Given
        Function<String, Parser> uncurriedParser = toParsers.apply(mock(InstructionResolver.class)).apply(mock(DirectionResolver.class));

        //When
        Parser parser = uncurriedParser.apply("1 2 N");

        //Then
        MowerParser mowerParser = (MowerParser) parser;
        assertThat(mowerParser.getType()).isEqualTo(MOWER_PARSER);
    }

    @Test
    public void toInstructionParserTest() {
        //Given
        InstructionResolver instructionResolver = mock(InstructionResolver.class);
        Mockito.when(instructionResolver.validate(any())).thenReturn(true);
        Function<String, Parser> uncurriedParser = toParsers.apply(instructionResolver).apply(mock(DirectionResolver.class));

        //When
        Parser parser = uncurriedParser.apply("GAGAGA");

        //Then
        InstructionsParser instructionsParser = (InstructionsParser) parser;
        assertThat(instructionsParser.getType()).isEqualTo(INSTRUCTION_PARSER);
    }


    @Test
    public void toIllegalParserTest() {
        //Given
        InstructionResolver instructionResolver = mock(InstructionResolver.class);
        Mockito.when(instructionResolver.validate(any())).thenReturn(false);
        Function<String, Parser> uncurriedParser = toParsers.apply(instructionResolver).apply(mock(DirectionResolver.class));
        String fileLine="Line not authorized";

        //When
        Parser parser = uncurriedParser.apply(fileLine);

        //Then
        IllegalParser illegalParser = (IllegalParser) parser;
        assertThat(illegalParser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
        assertThat(illegalParser.getFileLine()).isEqualTo(fileLine);
    }



    @Test
    public void toIllegalParserFromBadInstructionsTest() {
        //Given
        InstructionResolver instructionResolver = mock(InstructionResolver.class);
        Mockito.when(instructionResolver.validate(any())).thenReturn(false);
        Function<String, Parser> uncurriedParser = toParsers.apply(instructionResolver).apply(mock(DirectionResolver.class));
        String fileLine="GAGAGA";

        //When
        Parser parser = uncurriedParser.apply(fileLine);

        //Then
        IllegalParser illegalParser = (IllegalParser) parser;
        assertThat(illegalParser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
        assertThat(illegalParser.getFileLine()).isEqualTo(fileLine);
    }
}