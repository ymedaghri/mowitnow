package com.xebia.mowitnow.parser.lineparser;

import org.junit.Test;

import static com.xebia.mowitnow.parser.lineparser.ParserTypes.ILLEGAL_LINE_PARSER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Created by medaghrialaouiyoussef on 27/02/2017.
 */
public class IllegalParserTest {

    @Test
    public void getTypeTest() {
        //Given
        IllegalParser illegalParser = new IllegalParser("Line not authorized");

        //When -> Then
        assertThat(illegalParser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
    }

    @Test
    public void composeWithTest() {
        //Given
        IllegalParser illegalParser = new IllegalParser("Line not authorized");
        Parser anyParser = mock(Parser.class);

        //When
        Parser parser = illegalParser.composeWith(anyParser);

        //Then
        assertThat(parser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
        assertThat(parser).isEqualTo(illegalParser);
    }


    @Test
    public void bindToTest() {
        //Given
        IllegalParser illegalParser = new IllegalParser("Line not authorized");
        Parser anyParser = mock(Parser.class);

        //When
        Parser parser = illegalParser.bindTo(anyParser);

        //Then
        assertThat(parser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
        assertThat(parser).isEqualTo(illegalParser);
    }


}