package com.xebia.mowitnow.parser.lineparser;

import com.xebia.mowitnow.domain.direction.EnglishCardinalDirections;
import com.xebia.mowitnow.domain.mower.Mower;
import com.xebia.mowitnow.domain.position.Position;
import com.xebia.mowitnow.parser.resolver.DirectionResolver;
import org.junit.Test;

import java.util.Optional;

import static com.xebia.mowitnow.parser.lineparser.ParserTypes.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by medaghrialaouiyoussef on 27/02/2017.
 */
public class MowerParserTest {

    @Test
    public void getTypeTest() {
        //Given
        MowerParser parser = new MowerParser("1 2 N", mock(DirectionResolver.class));

        //When -> Then
        assertThat(parser.getType()).isEqualTo(MOWER_PARSER);
    }

    @Test
    public void composeWithTest() {
        //Given
        MowerParser mowerParser = new MowerParser("1 2 N", mock(DirectionResolver.class));
        Parser anyParser = mock(Parser.class);

        //When
        Parser parser = mowerParser.composeWith(anyParser);

        //Then
        assertThat(parser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
    }


    @Test
    public void bindToAllowedParserTest() {
        //Given
        DirectionResolver directionResolver = mock(DirectionResolver.class);
        when(directionResolver.findByCode("N")).thenReturn(Optional.of(EnglishCardinalDirections.NORTH));
        MowerParser mowerParser = new MowerParser("1 2 N", directionResolver);
        MowerZoneParser mowerZoneParser = new MowerZoneParser("5 5");

        //When
        MowerZoneParser parser = (MowerZoneParser) mowerParser.bindTo(mowerZoneParser);

        //Then
        assertThat(parser.getType()).isEqualTo(MOWER_ZONE_PARSER);
        Mower mower = parser.getAddedLast().get();
        assertThat(mower.getDirection()).isEqualTo(EnglishCardinalDirections.NORTH);
        assertThat(mower.getPosition()).isEqualTo(Position.of(1, 2));
    }


    @Test
    public void bindToNotAllowedParserTest() {
        //Given
        MowerParser mowerParser = new MowerParser("1 2 N", mock(DirectionResolver.class));
        Parser anyParser = mock(Parser.class);

        //When
        Parser parser = mowerParser.bindTo(anyParser);

        //Then
        assertThat(parser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
    }
}