package com.xebia.mowitnow.parser.lineparser;

import com.xebia.mowitnow.domain.position.Position;
import org.junit.Test;
import org.mockito.Mockito;

import static com.xebia.mowitnow.parser.lineparser.ParserTypes.ILLEGAL_LINE_PARSER;
import static com.xebia.mowitnow.parser.lineparser.ParserTypes.MOWER_ZONE_PARSER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Created by medaghrialaouiyoussef on 27/02/2017.
 */
public class MowerZoneParserTest {

    @Test
    public void zoneDefinitionTest() {
        //Given
        MowerZoneParser parser = new MowerZoneParser("5 5");

        //When -> Then
        assertThat(parser.getMowingZone().getLower()).isEqualTo(Position.of(0, 0));
        assertThat(parser.getMowingZone().getUpper()).isEqualTo(Position.of(5, 5));
    }

    @Test
    public void getTypeTest() {
        //Given
        MowerZoneParser parser = new MowerZoneParser("5 5");

        //When -> Then
        assertThat(parser.getType()).isEqualTo(MOWER_ZONE_PARSER);
    }

    @Test
    public void composeWithTest() {
        //Given
        MowerZoneParser mowerZoneParser = new MowerZoneParser("5 5");
        Parser anyParser = mock(Parser.class);

        //When
        mowerZoneParser.composeWith(anyParser);

        //Then
        Mockito.verify(anyParser).bindTo(mowerZoneParser);
    }


    @Test
    public void bindTo() {
        //Given
        MowerZoneParser mowerZoneParser = new MowerZoneParser("5 5");
        Parser anyParser = mock(Parser.class);

        //When
        Parser parser = mowerZoneParser.bindTo(anyParser);

        //Then
        assertThat(parser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
    }

}