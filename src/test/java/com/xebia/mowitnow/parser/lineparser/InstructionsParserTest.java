package com.xebia.mowitnow.parser.lineparser;

import com.xebia.mowitnow.domain.instruction.RotatingInstruction;
import com.xebia.mowitnow.domain.mower.Mower;
import com.xebia.mowitnow.parser.resolver.InstructionResolver;
import org.junit.Test;

import java.util.Optional;

import static com.xebia.mowitnow.parser.lineparser.ParserTypes.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

/**
 * Created by medaghrialaouiyoussef on 27/02/2017.
 */
public class InstructionsParserTest {

    @Test
    public void getTypeTest() {
        //Given
        InstructionsParser instructionsParser = new InstructionsParser("GAGAGA", mock(InstructionResolver.class));

        //When -> Then
        assertThat(instructionsParser.getType()).isEqualTo(INSTRUCTION_PARSER);
    }

    @Test
    public void composeWithTest() {
        //Given
        InstructionsParser instructionsParser = new InstructionsParser("GAGAGA", mock(InstructionResolver.class));
        Parser anyParser = mock(Parser.class);

        //When
        Parser parser = instructionsParser.composeWith(anyParser);

        //Then
        assertThat(parser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
    }


    @Test
    public void bindToAllowedParserTest() {
        //Given
        InstructionResolver instructionResolver = mock(InstructionResolver.class);
        when(instructionResolver.findByCode("G")).thenReturn(Optional.of(RotatingInstruction.G));
        InstructionsParser instructionsParser = new InstructionsParser("GGGGG", instructionResolver);
        Mower mower = mock(Mower.class);
        MowerZoneParser mowerZoneParser = new MowerZoneParser("5 5");
        mowerZoneParser.addMower(mower);

        //When
        Parser parser = instructionsParser.bindTo(mowerZoneParser);

        //Then
        assertThat(parser.getType()).isEqualTo(MOWER_ZONE_PARSER);
        verify(mower).addInstructions(anyList());
    }

    @Test
    public void bindToAllowedParserNotInitializedWithAMowerTest() {
        //Given
        InstructionResolver instructionResolver = mock(InstructionResolver.class);
        when(instructionResolver.findByCode("G")).thenReturn(Optional.of(RotatingInstruction.G));
        InstructionsParser instructionsParser = new InstructionsParser("GGGGG", instructionResolver);
        MowerZoneParser mowerZoneParser = new MowerZoneParser("5 5");

        //When
        Parser parser = instructionsParser.bindTo(mowerZoneParser);

        //Then
        assertThat(parser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
    }


    @Test
    public void bindToNotAllowedParserTest() {
        //Given
        InstructionsParser instructionsParser = new InstructionsParser("GGGGG", mock(InstructionResolver.class));
        Parser anyParser = mock(Parser.class);

        //When
        Parser parser = instructionsParser.bindTo(anyParser);

        //Then
        assertThat(parser.getType()).isEqualTo(ILLEGAL_LINE_PARSER);
    }

}