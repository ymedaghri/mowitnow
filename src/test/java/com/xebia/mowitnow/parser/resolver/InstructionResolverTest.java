package com.xebia.mowitnow.parser.resolver;

import com.xebia.mowitnow.domain.instruction.Instruction;
import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

import static com.xebia.mowitnow.domain.instruction.MovingInstruction.A;
import static com.xebia.mowitnow.domain.instruction.RotatingInstruction.D;
import static com.xebia.mowitnow.domain.instruction.RotatingInstruction.G;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by medaghrialaouiyoussef on 26/02/2017.
 */
public class InstructionResolverTest {

    @Test
    public void findByCodeTest() {
        //Given
        final Instruction[] listOfAvailableInstructions = {A, D, G};
        InstructionResolver instructionResolver = new InstructionResolver(listOfAvailableInstructions);

        //When -> Then
        assertThat(instructionResolver.findByCode("D")).isEqualTo(Optional.of(D));
        assertThat(instructionResolver.findByCode("E")).isEqualTo(Optional.empty());
    }

    @Test
    public void validateTest() {
        //Given
        final Instruction[] listOfAvailableInstructions = {A, D, G};
        InstructionResolver instructionResolver = new InstructionResolver(listOfAvailableInstructions);

        //When -> Then
        assertThat(instructionResolver.validate("DADADA")).isTrue();
        assertThat(instructionResolver.validate("DADADI")).isTrue();
    }

    @Test
    public void toInstructionsTest() {
        //Given
        final Instruction[] listOfAvailableInstructions = {A, D, G};
        InstructionResolver instructionResolver = new InstructionResolver(listOfAvailableInstructions);

        //When -> Then
        assertThat(instructionResolver.toInstructions("DADADA")).isEqualTo(asList(D, A, D, A, D, A));
    }
}