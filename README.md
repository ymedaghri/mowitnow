MowItNow Exercise
=================

This repository contains an implementation for the MowItNow Exercise.

### Versions

* Java 8
* Gradle 2+

## Gradle plugins

* Sonar plugin is configured to be used with a Docker instance of SonarQube.
* Jar plugin allows to create an executable jar
* Idea plugin

## Usage

    java -jar MowItNow.jar <Mower File Path>

A sample Mower File is present in the src/main/resources folder.
